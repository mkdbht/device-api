import Vue from 'vue'
import Router from 'vue-router'
import Home from "../pages/Home";
import About from "../pages/About";

Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/about',
        name: 'About',
        component: About
    },
    {
        path: '/device/:id',
        name: 'Device',
        component: Home
    },
    {
        path: '*',
        redirect: '/404'
    }
];

export default new Router({
    routes,
    mode: 'history'
})