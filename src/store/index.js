import Vue from 'vue'
import Vuex from 'vuex'
import devices from './devices'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        dark: false
    },
    getters: {
        dark: (state) => {
            return state.dark;
        }
    },
    mutations: {
        TOGGLE_THEME: (state) => {
            return state.dark = !state.dark;
        }
    },
    actions: {
        toggleTheme: ({commit}) => {
            commit('TOGGLE_THEME');
        }
    },
    modules: {
        devices
    }
});