import devices from '../../api/devices';

const actions = {
    getAllDevices: ({commit}) => {
        devices.getAllDevices().then(data => {
            commit('SET_DEVICES', data.devices)
        });
    },
    addDevice: ({commit}, payload) => {
        commit('ADD_DEVICE', payload)
    },

    sendDevices: ({commit, state}) => {
        var allDevices = state.devices.filter(item => {
            if (item.local === true) {
                delete item.local;
                return item;
            }
        });
        devices.postDevices(allDevices).then(response => {
            state.firstLoad = true;
            commit('SET_DEVICES', state.devices);
            commit('SET_SNACKBAR', {
                color: 'success',
                message: "Devices posted to server."
            });
        }).catch(error => {
            commit('SET_SNACKBAR', {
                color: 'error',
                message: error.response.data.message
            });
        });
    },

    deleteDevice: ({commit}, id) => {
        devices.deleteDevice(id).then(response => {
            commit('DELETE_DEVICE', id);
            commit('SET_SNACKBAR', {
                color: 'success',
                message: 'Device deleted successfully'
            });
        }).catch(error => {
            commit('DELETE_DEVICE', id);
            commit('SET_SNACKBAR', {
                color: 'error',
                message: error.response.data.message
            });
        });
    }
};

export default actions;