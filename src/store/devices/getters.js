const getters = {
    devices: (state) => {
        return state.devices;
    },
    name: (state) => {
        return state.name;
    },
    description: (state) => {
        return state.description;
    },

    snackbar: (state) => {
        return state.snackbar;
    },
    color: (state) => {
        return state.color;
    },
    message: (state) => {
        return state.message;
    }
};

export default getters;