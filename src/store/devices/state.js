const state = {
    devices: [],
    name: null,
    description: null,
    firstLoad: true,
    snackbar: false,
    color: 'success',
    message: ''
};

export default state;