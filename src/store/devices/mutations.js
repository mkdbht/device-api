import Vue from "vue";

const mutations = {
    SET_DEVICES: (state, payload) => {
        if (state.firstLoad === true) {
            state.firstLoad = false;
            state.devices = payload.map(item => {
                item.local = false;
                return item;
            });
        }
    },
    ADD_DEVICE: (state, payload) => {
        state.devices.push({
            id: Math.floor((Math.random() * 100000) + 1),
            name: payload.name,
            description: payload.description,
            local: true
        });
        state.name = '';
        state.description = '';
    },
    DELETE_DEVICE: (state, id) => {
        let devices = state.devices;
        let i = devices.findIndex(p => p.id == id);
        state.devices.splice(i, 1);
    },
    SET_NAME: (state, payload) => {
        state.name = payload
    },
    SET_DESCRIPTION: (state, payload) => {
        state.description = payload
    },
    SNACKBAR: (state) => {
        state.snackbar = !state.snackbar;
    },
    SET_SNACKBAR: (state, payload) => {
        state.snackbar = true;
        state.color = payload.color;
        state.message = payload.message;
    }
};

export default mutations;