import axios from 'axios';

export default {
    getAllDevices() {
        return axios.get('devices').then(response => {
            return response.data
        })
    },

    postDevices(data) {
        return axios.post('devices', {
            devices: data
        }).then(response => {
            return response;
        })
    },

    deleteDevice(id) {
        return axios.delete('devices/'+id).then(response => {
            return response;
        })
    }
}