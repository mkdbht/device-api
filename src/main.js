import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import router from './router';
import store from './store';
import axios from 'axios';
import i18n from './i18n'

Vue.use(Vuetify);
axios.defaults.baseURL = 'http://candidates.elsight-cloud.com:5001/api/v1';
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app');
